// @flow
// import type {TInvoiceState} from 'modules/invoices/reducers/invoices';
import type {TUserState} from 'modules/users/reducers/users';
import type {InvoiceApi} from 'modules/invoices/services/api';
import type {UserApi} from 'modules/users/services/api';

// from extra args
type TExtraArgs = {
    invoiceApi: InvoiceApi,
    userApi: UserApi,
};

type TPromiseAction = Promise<Object>;
type TGetState = () => any;
type TDispatch = (action: Object | TThunkAction | TPromiseAction | Array<Object>) => any;
export type TThunkAction = (dispatch: TDispatch, getState: TGetState, extraArgs: TExtraArgs) => any;

// from root reducer
export type TRootState = {
    invoice: {
        invoice: TInvoiceState,
    },
    user: {
        user: TUserState,
    },
};
