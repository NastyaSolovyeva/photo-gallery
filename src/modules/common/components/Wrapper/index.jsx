// @flow
import * as React from 'react';
import bemCn from 'bem-cn';
import './styles.less';

type TProps = {
    children?: React.Node,
};

const b = bemCn('wrapper');

export const Wrapper = (props: TProps) => {
    return <div className={b()}>{props.children}</div>;
};
