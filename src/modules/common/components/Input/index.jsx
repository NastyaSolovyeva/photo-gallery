// @flow
import * as React from 'react';
import {SyntheticEvent} from 'react';
import bemCn from 'bem-cn';
import './styles.less';

type TProps = {
    className?: string,
    id?: string,
    key?: string | number,
    label?: React.Node,
    name: string,
    onBlur?: (e: SyntheticEvent<HTMLInputElement>) => void,
    onChange?: (e: SyntheticEvent<HTMLInputElement>) => void,
    onFocus?: (e: SyntheticEvent<HTMLInputElement>) => void,
    placeholder?: string | null,
    required?: string | null,
    type?: string,
    value?: number | string | null,
};

const b = bemCn('form-input');

export default function FormInput({id, name, key, label, onChange, placeholder, required, type, value}: TProps): React.Node {
    const uniqueId = id ? id : name;
    const inputKey = key ? key : name;
    const [stateValue, setStateValue] = React.useState(undefined !== value ? value : '');
    const [isPlaceholderMinimized, setPlaceholderMinimized] = React.useState(Boolean(stateValue));
    const [isFocused, setIsFocused] = React.useState(false);

    const handleChange = React.useCallback(
        (e) => {
            e.preventDefault();

            if (onChange) {
                onChange(e);
            }
        },
        [onChange]
    );

    const handleFocus = React.useCallback(() => {
        setPlaceholderMinimized(true);
        setIsFocused(true);
    }, []);

    const handleBlur = React.useCallback(() => {
        setPlaceholderMinimized(false);
        setIsFocused(false);
    }, []);

    React.useEffect(() => {
        setStateValue(value);

        if (!value && !isFocused) {
            handleBlur();
        }
    }, [handleBlur, isFocused, value]);

    return (
        <div className={b()}>
            <div className={b('wrapper', {active: isFocused})}>
                <input
                    className={b('input')}
                    id={uniqueId}
                    key={inputKey}
                    name={name}
                    onBlur={handleBlur}
                    onChange={handleChange}
                    onFocus={handleFocus}
                    placeholder={placeholder}
                    required={required}
                    type={type}
                    value={stateValue}
                />
                {Boolean(label) && (
                    <label
                        className={b('label', {
                            active: Boolean(stateValue) || isPlaceholderMinimized,
                        })}
                        htmlFor={uniqueId}
                    >
                        {label}
                    </label>
                )}
            </div>
        </div>
    );
}

FormInput.defaultProps = {
    placeholder: '',
    required: '',
    type: 'text',
};
