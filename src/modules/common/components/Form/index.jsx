// @flow

import React from 'react';
import {Formik} from 'formik';
import * as Yup from 'yup';
import bemCn from 'bem-cn';
import Icon from 'antd/es/icon/index';
import FormInput from 'modules/common/components/Input';
import styles from './styles.less';

type TProps = {
    data?: Object,
    fieldList: Array<Object>,
    handleSubmit: Function,
    logoPath?: string,
    title?: string,
    validationSchema?: Yup.object,
};

const b = bemCn('auth-form');

export class Form extends React.Component<TProps> {
    constructor(props) {
        super(props);
        this.state = {
            active_input: false,
        };
    }

    handleFocus = () => {
        this.setState({active_input: true});
    };

    handleBlur = () => {
        this.setState({active_input: false});
    };

    renderForm = () => {
        const {data, fieldList, handleSubmit: handleSubmitGlobal, title, validationSchema} = this.props;
        return (
            <Formik enableReinitialize initialValues={data} onSubmit={handleSubmitGlobal} validationSchema={validationSchema}>
                {({values, errors, handleSubmit, handleChange, isSubmitting}) => (
                    <form className={b()} onSubmit={handleSubmit}>
                        <div className={b('logo')}>
                            <Icon type="deployment-unit" />
                        </div>
                        <div className={b('title')}> {title}</div>
                        {fieldList.map(({id, label, name, placeholder, required, type}) => (
                            <FormInput
                                className={b('input')}
                                id={id}
                                key={id}
                                label={label}
                                name={name}
                                onBlur={this.handleBlur}
                                onChange={handleChange}
                                onFocus={this.handleFocus}
                                placeholder={placeholder}
                                required={required}
                                type={type}
                                value={values[name] || ''}
                            />
                        ))}

                        {Object.keys(errors).map((errorItem) => (
                            <div className={styles.input_feedback} key={errorItem}>
                                {errors[errorItem]}
                            </div>
                        ))}
                        <button className={b('btn-submit')} disabled={isSubmitting} type="submit">
                            Login
                        </button>
                    </form>
                )}
            </Formik>
        );
    };

    render() {
        return this.renderForm();
    }
}
