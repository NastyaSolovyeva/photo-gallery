// @flow
import * as React from 'react';
import bemCn from 'bem-cn';
import {Link, withRouter} from 'react-router-dom';
import {compose} from 'redux';
import {PARAMS} from 'modules/common/constants/params';
import {Icon} from 'antd';
import {SideMenu} from 'modules/common/components/SideMenu';
import './styles.less';

type TProps = {
    children?: React.Node,
};

type TState = {
    isOpen: boolean,
};

const {
    VALUES: {HOME, ALBUMS, PHOTO, USERS},
} = PARAMS.HEADER;

const MENU_LIST = [
    {iconType: 'home', id: HOME, label: 'Home', url: '/'},
    {iconType: 'pic-right', id: ALBUMS, label: 'Album', url: `/${ALBUMS}`},
    {iconType: 'picture', id: PHOTO, label: 'Photo', url: `/${PHOTO}`},
    {iconType: 'user', id: USERS, label: 'Users', url: `/${USERS}`},
];

const b = bemCn('header');

class Header extends React.Component<TProps, TState> {
    menuRef = React.createRef();

    state = {
        isOpen: false,
    };

    closeMenuHandler = ({target}: any) => {
        const {isOpen} = this.state;

        if (target && isOpen && this.menuRef.current && !this.menuRef.current.contains(target)) {
            this.closeMenu();
        }
    };

    componentDidMount() {
        document.addEventListener('click', this.closeMenuHandler);
    }

    closeMenu = () => {
        this.setState({isOpen: false});
    };

    toggleMenu = () => {
        this.setState((state) => ({
            isOpen: !state.isOpen,
        }));
    };

    renderMenuItem = (className: string, withIcons: boolean) => {
        return MENU_LIST.map(({iconType, id, label, url}) => (
            <div className={b(className)} key={id}>
                {withIcons ? (
                    <div className={'side-menu-icon'}>
                        <Icon type={iconType} />
                    </div>
                ) : null}
                <Link to={url}>{label}</Link>
            </div>
        ));
    };

    renderSideMenu = () => {
        const {isOpen} = this.state;
        const withIcons = true;

        return (
            <SideMenu isOpen={isOpen}>
                <button className={b('close-icon')} onClick={this.closeMenu}>
                    <Icon type="close" />
                </button>
                {this.renderMenuItem('side-menu-item', withIcons)}
            </SideMenu>
        );
    };

    render() {
        return (
            <div className={b()}>
                <div className={b('menu')}>
                    {this.renderMenuItem('menu-item')}
                    <div className={b('hamburger-wrapper')}>
                        <button className={b('hamburger-btn')} onClick={this.toggleMenu}>
                            <Icon type="menu" />
                        </button>
                    </div>
                </div>
                {this.renderSideMenu()}
            </div>
        );
    }
}

const HeaderWrapper = compose(withRouter)(Header);

export {HeaderWrapper as Header};
