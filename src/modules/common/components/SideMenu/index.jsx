// @flow
import * as React from 'react';
import bemCn from 'bem-cn';
import './styles.less';

type TProps = {
    children?: any,
    isOpen?: boolean,
};

const b = bemCn('side-menu');

export const SideMenu = React.forwardRef(({children, isOpen}: TProps, ref) => {
    return (
        <div className={b({opened: isOpen})} ref={ref}>
            {children}
        </div>
    );
});

SideMenu.displayName = 'SideMenu';

SideMenu.defaultProps = {
    isOpen: false,
};
