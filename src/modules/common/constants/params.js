// @flow
export const PARAMS = {
    AUTH: {
        VALUES: {
            SIGN_IN: 'sign-in',
            SIGN_UP: 'sign-up',
        },
    },
    HEADER: {
        VALUES: {
            ALBUMS: 'albums',
            HOME: 'home',
            PHOTO: 'photo',
            USERS: 'users',
        },
    },
    USERS: {
        VALUES: {
            USERS: 'users',
        },
    },
};
