// @flow
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {compose} from 'redux';
import {getUser as getItemList} from 'modules/users/actions/index';
import type {TUserData} from 'modules/users/reducers/users';
import {selectUserData, selectUserIsLoading} from 'modules/users/selectors/index';
import {Table} from 'modules/common/components/Table/index';
import {Wrapper} from 'modules/common/components/Wrapper/index';
import {Header} from 'modules/common/components/Header';

type TProps = {
    getItemList: typeof getItemList,
    itemList: TUserData,
    itemListIsLoading: boolean,
};

class UsersContainer extends Component<TProps> {
    componentDidMount() {
        // const {getItemList, itemList, itemListIsLoading} = this.props; // !!!!!!!!!!!!!!!!
        const {itemList, itemListIsLoading} = this.props;

        if (!itemList.length && !itemListIsLoading) {
            getItemList();
        }
    }

    getDataColumns = () => {
        return [
            {
                dataIndex: 'name',
                key: 'name',
                title: 'Name',
            },
            {
                dataIndex: 'username',
                key: 'username',
                title: 'Username',
            },
        ];
    };

    render() {
        const columns = this.getDataColumns();

        return (
            <>
                <Header />
                <Wrapper>
                    <Table dataColumns={columns} dataList={this.props.itemList} />
                </Wrapper>
            </>
        );
    }
}

export const UsersLayout = compose(
    connect(
        (state) => {
            return {
                itemList: selectUserData(state),
                itemListIsLoading: selectUserIsLoading(state),
            };
        },
        {
            getItemList,
        }
    )
)(UsersContainer);
