// @flow
import {USER_ACTION_TYPE} from 'modules/users/constants/index';
import type {TThunkAction} from 'modules/types/index';
// import type {TUserItem, TUserData, TUserItemId} from 'modules/users/reducers/users';

// ------------ GET --------------
function getUserStart() {
    return {
        type: USER_ACTION_TYPE.GET_USER_START,
    };
}

function getUserSuccess(data) {
    return {
        payload: data,
        type: USER_ACTION_TYPE.GET_USER_SUCCESS,
    };
}

export function getUser(): TThunkAction {
    return async (dispatch, getState, {userApi}) => {
        dispatch(getUserStart());
        const data = await userApi.getUsers();
        dispatch(getUserSuccess(data));
    };
}
