// @flow
import {USER_ACTION_TYPE} from 'modules/users/constants';

export type TUserItemId = string;

export type TUserItem = {
    id: TUserItemId,
    name: string,
    username: string,
};

export type TUserData = Array<TUserItem>;

export type TUserState = $ReadOnly<{|
    data: TUserData,
    isLoading: boolean,
|}>;

type TUser = TUserData | TUserItem | TUserItemId;

export type TActionGetUserStart = {type: USER_ACTION_TYPE.GET_USER_START};
export type TActionGetUserFail = {type: USER_ACTION_TYPE.GET_USER_FAIL};
export type TActionGetUserSuccess = {payload: TUser, type: USER_ACTION_TYPE.GET_USER_SUCCESS};

type TUserAction = TActionGetUserStart | TActionGetUserFail | TActionGetUserSuccess;

export const initialState: TUserState = {
    data: [],
    isLoading: false,
};

const reducer = {
    // ------------------- GET -------------------
    [USER_ACTION_TYPE.GET_USER_FAIL](state) {
        return {
            ...state,
            isLoading: false,
        };
    },
    [USER_ACTION_TYPE.GET_USER_START](state) {
        return {
            ...state,
            isLoading: true,
        };
    },
    [USER_ACTION_TYPE.GET_USER_SUCCESS](state, {payload}) {
        return {
            ...state,
            data: payload,
            isLoading: false,
        };
    },
};

export function userReducer(state: TUserState = initialState, {payload, type}: TUserAction = {}) {
    if (reducer) {
        return type in reducer ? reducer[type](state, {payload, type}) : state;
    }
    return state;
}
