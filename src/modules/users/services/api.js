// @flow

export class UserApi {
    async getUsers() {
        // return await fetch('http://localhost:3000/users')
        return fetch('http://localhost:3000/users').then((response) => response.json());
    }
}
