// @flow
import {PARAMS} from 'modules/common/constants/params';

const AUTH_URL = '/auth';

export const AUTH_ROUTE = {
    SIGN_IN: `${AUTH_URL}/${PARAMS.AUTH.VALUES.SIGN_IN}`,
    SIGN_UP: `${AUTH_URL}/${PARAMS.AUTH.VALUES.SIGN_UP}`,
};
