// @flow
export const AUTH_MODULE_NAME: 'auth' = 'auth';

export const AUTH_ACTION_TYPE = {
    GET_AUTH_FAIL: 'AUTH__GET_AUTH_FAIL',
    GET_AUTH_START: 'AUTH__GET_AUTH_START',
    GET_AUTH_SUCCESS: 'AUTH__GET_AUTH_SUCCESS',

    LOG_OUT_FAIL: 'AUTH__LOG_OUT_FAIL',
    LOG_OUT_START: 'AUTH__LOG_OUT_START',
    LOG_OUT_SUCCESS: 'AUTH__LOG_OUT_SUCCESS',

    SIGN_UP_FAIL: 'AUTH__SIGN_UP_FAIL',
    SIGN_UP_START: 'AUTH__SIGN_UP_START',
    SIGN_UP_SUCCESS: 'AUTH__SIGN_UP_SUCCESS',
};
