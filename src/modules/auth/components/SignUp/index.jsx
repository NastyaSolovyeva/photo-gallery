// @flow
import * as React from 'react';
import {Form} from 'modules/common/components/Form';
import {AuthPage} from 'modules/auth/components/AuthPage';

const fieldList = [
    {
        id: 'name',
        label: 'Name',
        name: 'name',
        placeholder: '',
        required: 'required',
        type: 'text',
    },
    {
        id: 'email',
        label: 'Email',
        name: 'email',
        placeholder: '',
        required: 'required',
        type: 'text',
    },
    {
        id: 'phone',
        label: 'Phone',
        name: 'phone',
        placeholder: '',
        required: 'required',
        type: 'text',
    },
    {
        id: 'password',
        label: 'Password',
        name: 'password',
        placeholder: '',
        required: 'required',
        type: 'password',
    },
    {
        id: 'password_repeat',
        label: 'Repeat password',
        name: 'password_repeat',
        required: 'required',
        type: 'password',
    },
];

export function SignUp(): React.Node {
    const logoPath = 'img/logo.png';
    const title = 'Sign Up';

    return (
        <AuthPage>
            <Form fieldList={fieldList} logoPath={logoPath} title={title} />
        </AuthPage>
    );
}
