// @flow
import * as React from 'react';
// import {withRouter} from 'react-router-dom';
// import {compose} from 'redux';
import bemCn from 'bem-cn';

const b = bemCn('auth-page');

type TProps = {
    children?: React.Node,
};

export const AuthPage = (props: TProps) => {
    return <div className={b}>{props.children}</div>;
};
