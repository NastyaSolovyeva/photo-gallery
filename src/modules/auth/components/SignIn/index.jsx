// @flow
import * as React from 'react';
import {Form} from 'modules/common/components/Form';
import {AuthPage} from 'modules/auth/components/AuthPage';

const fieldList = [
    {
        id: 'email',
        label: 'Email',
        name: 'email',
        placeholder: '',
        required: 'required',
        type: 'text',
    },
    {
        id: 'password',
        label: 'Password',
        name: 'password',
        placeholder: '',
        required: 'required',
        type: 'password',
    },
];

export function SignIn(): React.Node {
    const logoPath = 'img/logo.png';
    const title = 'Log in';

    return (
        <AuthPage>
            <Form fieldList={fieldList} logoPath={logoPath} title={title} />
        </AuthPage>
    );
}
