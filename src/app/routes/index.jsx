// @flow
import * as React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {CreateLayout} from 'modules/invoices/components/CreateLayout';
import {EditLayout} from 'modules/invoices/components/EditLayout';
import {NoMatchRouter} from './NoMatchRouter';
import {itemId} from 'app/constants';
import {AUTH_ROUTE} from 'modules/auth/constants/routes';
import {SignIn} from 'modules/auth/components/SignIn';
import {SignUp} from 'modules/auth/components/SignUp';
import {UsersLayout} from 'modules/users/components/UsersLayout';

export const AppRouter = () => (
    <BrowserRouter>
        <Switch>
            <Route component={UsersLayout} exact path="/" />
            <Route component={CreateLayout} path="/create" />
            <Route component={EditLayout} path={'/edit/:' + itemId + '/'} />
            <Route component={SignIn} path={AUTH_ROUTE.SIGN_IN} />
            <Route component={SignUp} path={AUTH_ROUTE.SIGN_UP} />
            <Route component={UsersLayout} exact path="/users" />
            <Route component={NoMatchRouter} />
        </Switch>
    </BrowserRouter>
);
