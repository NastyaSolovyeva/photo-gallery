// @flow
import {combineReducers} from 'redux';
import {invoiceReducers} from 'modules/invoices/reducers';
import {userReducers} from 'modules/users/reducers/index';
import {INVOICE_MODULE_NAME} from 'modules/invoices/constants';
import {USER_MODULE_NAME} from 'modules/users/constants/';

export const rootReducer = combineReducers({
    [INVOICE_MODULE_NAME]: invoiceReducers,
    [USER_MODULE_NAME]: userReducers,
});
