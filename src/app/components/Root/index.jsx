// @flow
import {AppRouter} from 'app/routes';
import React from 'react';

export const RootContainer = () => <AppRouter />;
